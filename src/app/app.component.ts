import { Component, ViewChild  } from '@angular/core';
import {  Nav, Platform  } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from '../pages/home/home';
import { MatchesPage } from '../pages/matches/matches';
import { AboutPage } from '../pages/about/about';
import { NewsPage } from '../pages/news/news';
import { TeamPage } from '../pages/team/team';
import { GalleryPage } from '../pages/gallery/gallery';
import {Storage} from "@ionic/storage";

import {
  InAppBrowser,
  InAppBrowserOptions
} from "@ionic-native/in-app-browser";
import { PopoverController } from "ionic-angular";
import { LoginPage } from '../pages/login/login';
import { AuthProvider } from '../providers/auth/auth';
import { ItemsProvider } from "../providers/webitems/webitems";

import { LoadingController } from 'ionic-angular/components/loading/loading-controller';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;
  @ViewChild(Nav) nav : Nav;
  pages: Array<{title:string, icon: string, desc:string , class:string, component:any}>;
  activePage: any; 
  authenticated_user : any;
  is_loading = false;
  loggedInUser: String;
  data:any
  LogoImg:any;
  isLoading: boolean;
  connectionFailed = false;



  constructor(
   public platform: Platform,
   public statusBar: StatusBar, 
   splashScreen: SplashScreen, 
   private inAppBrowser: InAppBrowser,public storage: Storage,
   public popoverCtrl: PopoverController, public auth: AuthProvider, private _itemsSvc: ItemsProvider, public loadingCtrl: LoadingController, ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // let status bar overlay webview

      // set status bar to white
      this.statusBar.backgroundColorByHexString('#000');
      splashScreen.hide();
    });

    //this.authenticated_user.displayName=''

   


   


   this.pages = [  
              
             {title: 'Home', icon: 'home', class: 'mynclass',component: HomePage,desc:''},
             {title: 'Our Story', icon: 'contact', class: 'mynclass', component: AboutPage, desc: 'Club history and bio information'},
             {title: 'Club News', icon: 'medkit', class: 'mynclass', component: NewsPage , desc: 'Upcoming games and game updates throughtout the entire season'},
             {title: 'Team', class: 'myfavv', icon: 'star', component: TeamPage, desc: 'Club team and management'},
             {title: 'League', class: 'myfavv', icon: 'star', component: MatchesPage, desc: 'Game standings and updates'},
             {title: 'Gallaries', class: 'myfavv', icon: 'star', component: GalleryPage, desc: 'View club Gallaries'},
            
       ];

      this.storage.get('authenticatedUser').then(res=>{
      console.log(res)
        if ((res) === null)
         {
        console.log("logged in");
        this.rootPage = LoginPage;
         }
        else
        {
        console.log("already logged in");
        this.rootPage = HomePage;
         }
     this.loggedInUser = res?res.name:''; 
     
    });

    this._itemsSvc.getLogo().subscribe(
      res => {
        this.isLoading = false;
        this.LogoImg = res.nodes[0].node.field_featured_image.src;
        console.log(this.LogoImg);
    
      },
      err => {
        this.isLoading = false;
        if (err.name === "HttpErrorResponse") {
          this.connectionFailed = true;
        }
      }
    );
    
  }

 
  ionViewDidLoad() {
    console.log('ionViewDidLoad this page ');
  }  

  
   openPage(page){

    this.nav.setRoot(page.component);
    this.activePage = page;

  }

  public checkActivePage(page): boolean{
    return page === this.activePage;
  }

 openUrl(url: string) {
    const options: InAppBrowserOptions = {
      zoom: "no"
    };
    // Opening a URL and returning an InAppBrowserObject
    const browser = this.inAppBrowser.create(url, "_system", options);
  }



logout() {

  let loading = this.loadingCtrl.create({
    content: 'logging out ...'
  });
  
  localStorage.clear();

  loading.present();
  this.auth.signOut()
  this.nav.setRoot(LoginPage);
  loading.dismiss();
}
}
