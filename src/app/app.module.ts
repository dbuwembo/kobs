import { PopoverPage } from "./../pages/popover/popover";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { NewsDetailsPage } from "./../pages/news-details/news-details";
import { MatchDetailsPage } from "./../pages/match-details/match-details";
import {Youtube} from "../pipes/youtube";

import { MatchesPage } from "./../pages/matches/matches";
import { UcupPage } from "./../pages/ucup/ucup";
import { SuperPage } from "./../pages/super/super";
import { PremierPage } from "./../pages/premier/premier";
import { NewsPage } from "./../pages/news/news";
import { TeamPage } from "./../pages/team/team";
import { GalleryPage } from "./../pages/gallery/gallery";

import { NgModule, ErrorHandler } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { IonicApp, IonicModule, IonicErrorHandler } from "ionic-angular";
import { MyApp } from "./app.component";
import { HttpClientModule } from "@angular/common/http";
import { HttpModule } from '@angular/http';

import { IonicStorageModule } from "@ionic/storage";
import { AboutPage } from "../pages/about/about";
import { HomePage } from "../pages/home/home";
import { TabsPage } from "../pages/tabs/tabs";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { NewsProvider } from "../providers/news/news";
import { ItemsProvider } from "../providers/webitems/webitems";

import { SocialSharing } from "@ionic-native/social-sharing";
import { LoginPage } from "../pages/login/login";
import { VerifyPage } from "../pages/verify/verify";



import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

// //import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFirestoreModule} from 'angularfire2/firestore'

import { AuthProvider } from '../providers/auth/auth';
import { ForgotPasswordPage } from "../pages/forgot-password/forgot-password";
import { RegisterPage } from "../pages/register/register";

export const firebaseConfig = {
apiKey: "AIzaSyCTzgsrTuWyFHX7-O43bQWNl-N240q0Yr4",
authDomain: "kobs-app.firebaseapp.com",
databaseURL: "https://kobs-app.firebaseio.com",
projectId: "kobs-app",
storageBucket: "kobs-app.appspot.com",
messagingSenderId: "626105213877"
};

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    HomePage,
    TabsPage,
    NewsPage,
    PremierPage,
    MatchesPage,
    NewsDetailsPage,
    SuperPage,
    Youtube,
    UcupPage,
    GalleryPage,
    MatchDetailsPage,
    TeamPage,
    PopoverPage,
    LoginPage,
    ForgotPasswordPage,
    RegisterPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    MatchDetailsPage,
    PremierPage,
    SuperPage,
    TeamPage,
    UcupPage,
    HomePage,
    TabsPage,
    GalleryPage,
    NewsPage,
    MatchesPage,
    NewsDetailsPage,
    PopoverPage,
    LoginPage,
    ForgotPasswordPage,
    RegisterPage
 
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    NewsProvider,
    ItemsProvider,
    SocialSharing,
    InAppBrowser,
    AngularFireAuth,
    AuthProvider
  ]
})
export class AppModule {}
