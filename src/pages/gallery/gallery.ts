import { NewsDetailsPage } from "./../news-details/news-details";
import { ItemsProvider } from "../../providers/webitems/webitems";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { LoadingController } from "ionic-angular/components/loading/loading-controller";
import * as $ from "jquery";
import 'slick-carousel/slick/slick';

import 'rxjs/add/operator/map';
import {Http, Headers, RequestOptions}  from "@angular/http";
import {Storage} from '@ionic/storage';

@IonicPage()
@Component({
  selector: "page-gallery",
  templateUrl: "gallery.html"
})
export class GalleryPage {
  gallery: any;
  isLoading: boolean;
  currentPage = 0;
  connectionFailed = false;
  newItems: any
  errorMessage: string;
  page = 0;
  toggled: boolean;
  loggedInUser:any;
  loggedInNumber:any;
  loggedInEmail:any;
  newsTitle:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _itemsSvc: ItemsProvider,
    public loadingCtrl: LoadingController,
    private http: Http,
    private storage: Storage
  ) {}

  ionViewDidLoad() {
    this.getGallaries();
    this.storage.get('authenticatedUser').then(res=>{
      console.log(res)
        
        
     this.loggedInUser = res?res.name:''; 
     this.loggedInNumber = res?res.phonenumber:'';
     this.loggedInEmail = res?res.email:'';
     this.newsTitle = 'Club Gallery';
      var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let data = {
        name: this.loggedInUser,
        data: this.newsTitle,
        mobile: this.loggedInNumber,
        clubid: 'kobs',
        type: 'GalleryPage'
      };
    this.http.post('http://bet-ways.com/appdata/audit.php',data, options)
    .map(res => res.json())
    .subscribe(res => {
     console.log(res); 

  });
      
  });
  }

  getGallaries() {
    this.isLoading = true;
    let loader = this.loadingCtrl.create({
      spinner: 'hide',
  cssClass: 'my-loading-class',
    content: `
      <div class="custom-spinner-container">
        <div class="custom-spinner-box ">
           <img class="animated bounceIn" src="assets/imgs/icon.png" />
        </div>
        <div class="overlay"></div>
      </div>`,
    duration: 5000
    });
    loader.present();
    this._itemsSvc.getGallaries().subscribe(
      res => {
        this.isLoading = false;
        this.gallery = res.nodes;
                setTimeout(() => {
          $('.myCarousel').slick({
      dots: false,
      centerMode: false,
      infinite: true,
      centerPadding: '10px',
      slidesToShow: 1,
      arrows:true,
      autoplay:false
    });
        }, 500);
        loader.dismiss();
      },
      err => {
        this.isLoading = false;
        if (err.name === "HttpErrorResponse") {
          this.connectionFailed = true;
        }
        console.log(err.name);
        loader.dismiss();
      }
    );
  }

  openPage(data) {
    console.log(data);
    this.navCtrl.push(NewsDetailsPage, { details: data });
  }

  doRefresh(refresher) {
    this._itemsSvc.getGallaries().subscribe(
      res => { 
        this.isLoading = false;
        this.gallery = res.nodes;
        setTimeout(() => {
          $('.myCarousel').slick({
      dots: false,
      centerMode: false,
      infinite: true,
      centerPadding: '10px',
      slidesToShow: 1,
      arrows:true,
      autoplay:false
    });
        }, 500);
        refresher.complete();
      },
      err => {
        this.isLoading = false;
        refresher.complete();
        if (err.name === "HttpErrorResponse") {
          this.connectionFailed = true;
        }
        console.log(err.name);
      }
    );
  }

   

}
