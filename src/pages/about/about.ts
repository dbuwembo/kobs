import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { LoadingController } from "ionic-angular/components/loading/loading-controller";
import { ItemsProvider } from "../../providers/webitems/webitems";

import 'rxjs/add/operator/map';
import {Http, Headers, RequestOptions}  from "@angular/http";
import {Storage} from '@ionic/storage';
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  
  mybody:any
  isLoading: boolean;
  connectionFailed = false;
  loggedInUser:any;
  loggedInNumber:any;
  loggedInEmail:any;
  newsTitle:any;
  constructor(
  	public navCtrl: NavController, 
    public navParams: NavParams,
  	private _itemsSvc: ItemsProvider,
    public loadingCtrl: LoadingController,
    private http: Http,
    private storage: Storage) {

  }


  ionViewDidLoad() {
    this.refresh();

        this.storage.get('authenticatedUser').then(res=>{
      console.log(res)
        
        
     this.loggedInUser = res?res.name:''; 
     this.loggedInNumber = res?res.phonenumber:'';
     this.loggedInEmail = res?res.email:'';
     this.newsTitle = 'About Club';
      var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let data = {
        name: this.loggedInUser,
        data: this.newsTitle,
        mobile: this.loggedInNumber,
        clubid: 'kobs',
        type: 'aboutpage'
      };
    this.http.post('http://bet-ways.com/appdata/audit.php',data, options)
    .map(res => res.json())
    .subscribe(res => {
     console.log(res); 

  });
      
    });
  }
    getAbout() {

    this._itemsSvc.getAbout().subscribe(
      res => {
        this.isLoading = false;
        this.mybody = res.nodes[0].node.body;
        
      },
      err => {
        this.isLoading = false;
        if (err.name === "HttpErrorResponse") {
          this.connectionFailed = true;
        }
      }
    );


  }
   
     refresh() {
    this.getAbout();
 

  }

}
