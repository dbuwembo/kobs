import { Component } from "@angular/core";
import { NavController , MenuController} from "ionic-angular";
import { ItemsProvider } from "../../providers/webitems/webitems";
import { LoadingController } from "ionic-angular";
import { NewsDetailsPage } from "../news-details/news-details";

import {Http, Headers, RequestOptions}  from "@angular/http";
import {Storage} from '@ionic/storage';
import "rxjs/add/operator/mergeMap";
import * as $ from "jquery";
import 'slick-carousel/slick/slick';

import {
  InAppBrowser,
  InAppBrowserOptions
} from "@ionic-native/in-app-browser";
import { PopoverController } from "ionic-angular";

@Component({
  selector: "page-home", 
  templateUrl: "home.html"
})
export class HomePage {
 
  isLoading: boolean;
  currentPage = 0;
  connectionFailed = false;
  latestdate: any
        lhomeClub: any
        lhomeresult: any
        lhomeImg: any 
        lawayClub: any 
        lawayresult: any 
        lawayImg: any 
        resultsi: any
        trophies: any
        resultsicon: any 
        gamesicon: any
        natestdate: any 
        nhomeClub: any 
        nhomeresult: any 
        nhomeImg: any
        nawayClub: any 
        nawayresult: any
        nawayImg: any 

        unatestdate: any 
        unhomeClub: any 
        unhomeresult: any 
        unhomeImg: any
        unawayClub: any 
        unawayresult: any
        unawayImg: any
        counter: any
  myslides: any
  myclubs: any
  myteam: any
  news: any
  items = [];
  node:  string[];
  newItems:any;
  event:any;
  video:any;
  loggedInUser:any;
  loggedInNumber:any;
  loggedInEmail:any;
  newsTitle:any;

  loading = this.loadingCtrl.create({
  spinner: 'hide',
  cssClass: 'my-loading-class',
    content: `
      <div class="custom-spinner-container">
        <div class="custom-spinner-box ">
           <img class="animated bounceIn" src="assets/imgs/icon.png" />
        </div>
        <div class="overlay"></div>
      </div>`,
    duration: 5000
  });



  constructor(
    public navCtrl: NavController,
    private _itemsSvc: ItemsProvider,
    public loadingCtrl: LoadingController,
    private inAppBrowser: InAppBrowser,
    public popoverCtrl: PopoverController,
    private menuCtrl: MenuController,
    private http: Http,
    private storage: Storage
  ) {this.menuCtrl.enable(true);}




  ionViewDidLoad() {

    
    this.loading.present();
    this.refresh();

      this.storage.get('authenticatedUser').then(res=>{
      console.log(res)
        
        
     this.loggedInUser = res?res.name:''; 
     this.loggedInNumber = res?res.phonenumber:'';
     this.loggedInEmail = res?res.email:'';
     this.newsTitle = 'Accessed Home';
      var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let data = {
        name: this.loggedInUser,
        data: this.newsTitle,
        mobile: this.loggedInNumber,
        clubid: 'kobs',
        type: 'Home'
      };
    this.http.post('http://bet-ways.com/appdata/audit.php',data, options)
    .map(res => res.json())
    .subscribe(res => {
     console.log(res); 

  });
      
    });
    
    
  }




//  getPremierSingle() {

 //   this._itemsSvc.getPremierSingle().subscribe(
 //     res => {
  //      this.isLoading = false;
  //      this.latestdate = res.nodes[0].node.field_date_of_game;
  //      this.lhomeClub = res.nodes[0].node.field_home_club;
  //      this.lhomeresult = res.nodes[0].node.field_home_result;
  //      this.lhomeImg = res.nodes[0].node.field_club_logo.src;
 //       this.lawayClub = res.nodes[0].node.field_away_club;
 //       this.lawayresult = res.nodes[0].node.field_away_result;
 //       this.lawayImg = res.nodes[0].node.awayclublogo.src;
  //    },
  //    err => {
 //       this.isLoading = false;
  //      if (err.name === "HttpErrorResponse") {
 //         this.connectionFailed = true;
  //      }
  //    }
  //  );
 // }
 
  getPremierSingle() {

    this._itemsSvc.getNextMatch().subscribe(
      res => {
        this.isLoading = false;
        this.latestdate = res.nodes[0].node.field_date_of_game;
        this.lhomeClub = res.nodes[0].node.field_home_club;
        this.lhomeresult = res.nodes[0].node.field_home_result;
        this.lhomeImg = res.nodes[0].node.field_club_logo.src;
        this.lawayClub = res.nodes[0].node.field_away_club;
        this.lawayresult = res.nodes[0].node.field_away_result;
        this.lawayImg = res.nodes[0].node.awayclublogo.src;

    
      },
      err => {
        this.isLoading = false;
        if (err.name === "HttpErrorResponse") {
          this.connectionFailed = true;
        }
      }
    );
  }
   

  getSuperSingle() {

    this._itemsSvc.getSuperSingle().subscribe(
      res => {
        this.isLoading = false;
        this.natestdate = res.nodes[0].node.field_date_of_game;
        this.nhomeClub = res.nodes[0].node.field_home_club;
        this.nhomeresult = res.nodes[0].node.field_home_result;
        this.nhomeImg = res.nodes[0].node.field_club_logo.src;
        this.nawayClub = res.nodes[0].node.field_away_club;
        this.nawayresult = res.nodes[0].node.field_away_result;
        this.nawayImg = res.nodes[0].node.awayclublogo.src;
      },
      err => {
        this.isLoading = false;
        if (err.name === "HttpErrorResponse") {
          this.connectionFailed = true;
        }
      }
    );
  }

  getUcupSingle() {

    this._itemsSvc.getUcupSingle().subscribe(
      res => {
        this.isLoading = false;
        this.unatestdate = res.nodes[0].node.field_date_of_game;
        this.unhomeClub = res.nodes[0].node.field_home_club;
        this.unhomeresult = res.nodes[0].node.field_home_result;
        this.unhomeImg = res.nodes[0].node.field_club_logo.src;
        this.unawayClub = res.nodes[0].node.field_away_club;
        this.unawayresult = res.nodes[0].node.field_away_result;
        this.unawayImg = res.nodes[0].node.awayclublogo.src;
      },
      err => {
        this.isLoading = false;
        if (err.name === "HttpErrorResponse") {
          this.connectionFailed = true;
        }
      }
    );
  }



   getAllTeam(){
    
    this._itemsSvc.getAllTeam().subscribe(
      res => {
        this.isLoading = false;
        this.myteam = res.nodes;
        //console.log(this.myteam);
        setTimeout(() => {
          $('.myCarousel2').slick({
      dots: false,
      centerMode: false,
      infinite: false,
      centerPadding: '10px',
      slidesToShow: 2,
      arrows:true,
      autoplay:false
    });
        }, 500);
      },
      err => {
        this.isLoading = false;
        if (err.name === "HttpErrorResponse") {
          this.connectionFailed = true;
        }
      }
    );

  }

   


   getNewsList(){
    
    this._itemsSvc.getNewsList().subscribe(
      res => {
        this.isLoading = false;
        this.news = res.nodes;
        //console.log(this.news);
       this.loading.dismiss();

        
      },
      err => {
        this.isLoading = false;
        if (err.name === "HttpErrorResponse") {
          this.connectionFailed = true;
        }
      }
    );

  }


  getLatestVideo(){
    
    this._itemsSvc.getLatestVideo().subscribe(
      res => {
        this.isLoading = false;
        this.video = res.nodes;
        console.log(this.video);

        
      },
      err => {
        this.isLoading = false;
        if (err.name === "HttpErrorResponse") {
          this.connectionFailed = true;
        }
      }
    );

  }

  

  refresh() {
    this.getPremierSingle();
    this.getSuperSingle();
    this.getNewsList();
    this.getUcupSingle();
    this.getLatestVideo();

  }

 
  openPage(data) {
    this.navCtrl.push(NewsDetailsPage, { details: data });
  }

  openUrl(url: string) {
    const options: InAppBrowserOptions = {
      zoom: "no"
    };
    // Opening a URL and returning an InAppBrowserObject
    this.inAppBrowser.create(url, "_system", options);
  }

  
}
