import { MatchDetailsPage } from './../match-details/match-details';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ItemsProvider } from "../../providers/webitems/webitems";
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';

@IonicPage()
@Component({
  selector: 'page-ucup',
  templateUrl: 'ucup.html',
})
export class UcupPage {
  matches: any;
  isLoading: boolean;
  currentPage = 0;
  connectionFailed = false;
  newItems: any
  errorMessage: string;
  page = 0;
  toggled: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, private _itemsSvc: ItemsProvider, public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    this.getUcupAll();
  }

  getUcupAll() {
    this.isLoading = true;
    let loader = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loader.present();
    this._itemsSvc.getUcupAll(this.currentPage).subscribe(res => {
      this.isLoading = false;
      this.matches = res.nodes;
      loader.dismiss();
    }, err => {
      this.isLoading = false;
      if (err.name === 'HttpErrorResponse') {
        this.connectionFailed = true;
      }
      console.log(err.name);
      loader.dismiss();
    });
  }

  openPage(data) {
    console.log(data);
    this.navCtrl.push(MatchDetailsPage, { details: data })
  }

  doRefresh(refresher) {
    this._itemsSvc.getUcupAll(this.currentPage).subscribe(res => {
      this.isLoading = false;
      this.matches = res.nodes;
      refresher.complete();
    }, err => {
      this.isLoading = false;
      refresher.complete();
      if (err.name === 'HttpErrorResponse') {
        this.connectionFailed = true;
      }
      console.log(err.name);
    });
  }

    //load all items if end is reached
    doInfinite(infiniteScroll) {
    this.page = this.page+1;
    setTimeout(() => {
      this._itemsSvc.getUcupAll(this.page)
         .subscribe(
           res => {
             this.newItems = res.nodes;
             for(let i=0; i<this.newItems.length; i++) {
               this.matches.push(this.newItems[i]);
             }
           },
           error =>  this.errorMessage = <any>error);

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 1000);
  }

}
