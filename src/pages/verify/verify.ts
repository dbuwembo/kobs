import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController , MenuController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoadingController } from 'ionic-angular';

import {Http, Headers, RequestOptions}  from "@angular/http";
import 'rxjs/add/operator/map';
import { ItemsProvider } from "../../providers/webitems/webitems";

/**
 * Generated class for the VerifyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-verify',
  templateUrl: 'verify.html',
})
export class VerifyPage {
VerifyCodeForm: FormGroup;
  loginError: string;
  alert_title = ''
  response: any;
  LogoImg:any;
  isLoading: boolean;
  connectionFailed = false;


  @ViewChild("code") code;
  constructor(public navCtrl: NavController,
    public navParams: NavParams, 
    fb: FormBuilder, 
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private http: Http,
    private menuCtrl: MenuController,
    private _itemsSvc: ItemsProvider) {
    this.VerifyCodeForm = fb.group({
    code: ['', [Validators.required]]
    });
    this.menuCtrl.enable(false);
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad VerifyPage');
  }



}
