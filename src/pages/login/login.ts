import { Component , ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController , MenuController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { HomePage } from '../home/home';
import { LoadingController } from 'ionic-angular';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';
import { RegisterPage } from '../register/register';
import { stringify } from 'querystring';
import 'rxjs/add/operator/map';
import {Http, Headers, RequestOptions}  from "@angular/http";
import {Storage} from '@ionic/storage';
import { ItemsProvider } from "../../providers/webitems/webitems";






/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loginForm: FormGroup;
  loginError: string;
  response: any;
  @ViewChild("mobilenumber") mobilenumber;
  @ViewChild("password") password;
  @ViewChild("clubid") clubid;
  data:string;
  items:any;
  LogoImg:any;
  isLoading: boolean;
  connectionFailed = false;
  
  

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams, 
    fb: FormBuilder, 
    private auth: AuthProvider, 
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private http: Http,
    private menuCtrl: MenuController,
    private _itemsSvc: ItemsProvider,
    private storage: Storage) {
    this.loginForm = fb.group({
    mobilenumber: ['', [Validators.required]],
    clubid: ["", [Validators.required]],
    password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    });
    this.menuCtrl.enable(false);
  }

  ionViewDidLoad() {
   // console.log('ionViewDidLoad LoginPage');
   this.getLogo();
  }

  getLogo() {

    this._itemsSvc.getLogo().subscribe(
      res => {
        this.isLoading = false;
        this.LogoImg = res.nodes[0].node.field_featured_image.src;
    
      },
      err => {
        this.isLoading = false;
        if (err.name === "HttpErrorResponse") {
          this.connectionFailed = true;
        }
      }
    );
  }
   
    
  login(){

  //// check to confirm the username and password fields are filled
 
  if(this.mobilenumber.value=="" ){

 let alert = this.alertCtrl.create({

 title:"ATTENTION",
 subTitle:"Mobile Number field is empty",
 buttons: ['OK']
 });

 alert.present();
  } else

 if(this.password.value==""){

 let alert = this.alertCtrl.create({

 title:"ATTENTION",
 subTitle:"Password field is empty",
 buttons: ['OK']
 });

 alert.present();
      
}
 else
 {

  var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });


      let data = {
        username: this.mobilenumber.value,
        clubid: this.clubid.value,
        password: this.password.value
      };

      

 let loader = this.loadingCtrl.create({
    content: 'Processing please wait...',
  });

 loader.present().then(() => {


  this.http.post('http://bet-ways.com/appdata/login.php',data,options)
  .map(res => res.json())
  .subscribe(res => {
  console.log(res)
  localStorage.setItem('kobs_user7',JSON.stringify(res))
  this.storage.set('authenticatedUser_id', res.id);
  this.storage.set('authenticatedUser', res);
   loader.dismiss()
  if(res.club=="kobs"){
   
    let alert = this.alertCtrl.create({
      title:"CONGRATS",
      subTitle:"Sucessfully Logged In",
      buttons: ['OK']
      });
     
      alert.present();
      
      this.navCtrl.setRoot(HomePage);
  }else
  {
   let alert = this.alertCtrl.create({
   title:"ERROR",
   subTitle:"Your Login Username or Password is invalid",
   buttons: ['OK']
   });
  
   alert.present();
    } 
  });
  });
   }
  
  }
 
  /*login() {

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
  
    loading.present();
  

    let data = this.loginForm.value;

    if (!data.email) {
      return;
    }

    let credentials = {
      email: data.email,
      password: data.password
    };

  

    this.auth.signInWithEmail(credentials)
      .then(

        res => {
          //console.log(response.user);
          localStorage.setItem('kobs_user7',JSON.stringify(res.user))
          this.response = res.user

          const audit_trail = {
            'lastlogin': new Date(),
            'name': this.response.displayName,
            'email': this.response.email,
            'user_id': this.response.uid
          }

    

          this.createAuditTrail(audit_trail); 
          
          loading.dismiss();
          this.navCtrl.setRoot(HomePage);
          
        },
      //  () => this.navCtrl.setRoot(HomePage),
        error => {
          this.loginError = error.message;
         // console.log(error);
          switch(error.code){
            case 'auth/user-not-found':
            this.loginError = 'invalid username or password';
            loading.dismiss();
            this.presentAlert();
            break;

            case 'auth/invalid-email':
            this.loginError = 'invalid email address';
            loading.dismiss();
            this.presentAlert();
            break;

            case 'auth/wrong-password':
            this.loginError = 'invalid password';
            loading.dismiss();
            this.presentAlert();
            break;
          }
        }
        
      );


  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Login Error',
      subTitle:  this.loginError,
      buttons: ['Dismiss']
    });
    alert.present();
  }*/

  showForgotPassword(){
    this.navCtrl.push(ForgotPasswordPage);
  }


  showregisterUser(){
    this.navCtrl.push(RegisterPage);
  }

  createAuditTrail(data){
     this.auth.updateAuditTrail(data).then(

      res =>{
        console.log(res)
      },

      err =>{
        console.log(err)
      }
     )
  }
}
