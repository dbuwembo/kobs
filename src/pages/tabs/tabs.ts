import { NewsPage } from './../news/news';
import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { MatchesPage } from '../matches/matches';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = NewsPage;
  tab3Root = MatchesPage;

  constructor() {

  }

}
