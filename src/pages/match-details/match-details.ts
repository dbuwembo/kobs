import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";

import 'rxjs/add/operator/map';
import {Http, Headers, RequestOptions}  from "@angular/http";
import {Storage} from '@ionic/storage';
@IonicPage()
@Component({
  selector: "page-match-details",
  templateUrl: "match-details.html"
})
export class MatchDetailsPage {
  newsBody: any;
  newsTitle = "OFFLINE!";
  newsCreated: any;
  newsImg: any;
  newsTitle1 = "OFFLINE!";

  loggedInUser:any;
  loggedInNumber:any;
  loggedInEmail:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private http: Http,
    private storage: Storage
  ) {}

  ionViewDidLoad() {

        this.storage.get('authenticatedUser').then(res=>{
      console.log(res)
        
        
     this.loggedInUser = res?res.name:''; 
     this.loggedInNumber = res?res.phonenumber:'';
     this.loggedInEmail = res?res.email:'';
     this.newsTitle1 = this.navParams.data.details["node"].matchtitle;
      var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let data = {
        name: this.loggedInUser,
        data: this.newsTitle1,
        mobile: this.loggedInNumber,
        clubid: 'kobs',
        type: 'Game Results'

      };
    this.http.post('http://bet-ways.com/appdata/audit.php',data, options)
    .map(res => res.json())
    .subscribe(res => {
     console.log(res); 

  }); 

  });

    this.newsBody = this.navParams.data.details["node"].body;
    this.newsCreated = this.navParams.data.details["node"].field_date_of_game;
    this.newsTitle = this.navParams.data.details["node"].matchtitle;
    this.newsImg = this.navParams.data.details["node"].field_featured_image.src;
  }


}
