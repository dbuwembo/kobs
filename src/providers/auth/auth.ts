import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { AngularFirestore } from 'angularfire2/firestore';



// //import AuthService = firebase.auth.AuthProvider;

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {
  private user: firebase.User;

  constructor(public afAuth: AngularFireAuth, public afs: AngularFirestore) {
    //console.log('Hello AuthProvider Provider');

    afAuth.authState.subscribe(user => {
			this.user = user;
		});
  }


	signInWithEmail(credentials) {
		//console.log('Sign in with email');
		return this.afAuth.auth.signInWithEmailAndPassword(credentials.email,
			 credentials.password);
	}

  signUp(credentials) {
    return this.afAuth.auth.createUserWithEmailAndPassword(credentials.email, credentials.password);
  }

  resetPasswordInit(email: string) { 
    return this.afAuth.auth.sendPasswordResetEmail(
      email, 
      ); 
    } 

  updateUserProfile(data: string) { 
      return this.afAuth.auth.currentUser.updateProfile(
        {
          displayName: data,
         // photoURL: 'some/url'
        }
        //email, 
        ); 
      } 

  signOut(): Promise<void> {
        return this.afAuth.auth.signOut();
      }

  updateAuditTrail(data ){
     return this.afs.collection('audit-trail').add(data);
  }

  addMember(data ){
    return this.afs.collection('members').add(data);
 }
}
